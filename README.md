`static-server-start.sh` starts NGINX to serve the contents of the `static-server` folder. There is an HTML page there with "Test HTML" as the body.
Confirm by running `curl localhost:8080`

`conf-server-start.sh` starts NGINX with the configuration `conf-nginx.conf` (which is the default) and serving a site `conf-server.conf`. This has a path `/app` that will serve HTTP 200 with body "Whoo!".
Confirm by running `curl localhost:8081/app`
