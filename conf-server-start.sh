docker run \
	--name conf-nginx \
	-v D:/Temp/StaticServer/conf-nginx.conf:/etc/nginx/nginx.conf:ro \
	-v D:/Temp/StaticServer/conf-server.conf:/etc/nginx/conf.d/custom-server.conf:ro \
	-d \
	-p 8081:80 \
	nginx
